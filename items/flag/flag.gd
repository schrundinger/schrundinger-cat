extends Area2D

signal goal(flag_name)

var raised=false

func _on_flag_body_enter( body ):
	if (not raised and body extends preload("res://crew/player.gd")):
		get_node("anim").play('raise')
		raised=true
		emit_signal("goal", get_name())

func _ready():
	pass
