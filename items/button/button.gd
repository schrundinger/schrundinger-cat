extends Area2D

signal button_pressed
var pressed=false

func _on_button_body_enter( body ):
	if (not pressed and body extends preload("res://crew/player.gd")):
		#var imagetexture = ImageTexture.new()
		#imagetexture.load("res://assets/Other/flagRed_up.png")
		get_node("sprite").set_frame(1)
		emit_signal('button_pressed')
		pressed=true

func _ready():
	pass
