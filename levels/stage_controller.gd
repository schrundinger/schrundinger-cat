extends Node

signal die(world)
signal win(world)

var world = ""

func _ready():
	var enemies_group = get_node("enemies")
	if enemies_group:
		for e in enemies_group.get_children():
			e.connect("touched", self, "_on_kill_area_body_enter")

	var goals_group = get_node("goals")
	for g in goals_group.get_children():
		g.connect("goal", self, "_on_goal_player_enter")
		
	var kill_areas_groups = get_node("kill_areas")
	if kill_areas_groups:
		for d in kill_areas_groups.get_children():
			d.connect("body_enter", self, "_on_kill_area_body_enter")

func _on_goal_player_enter(flag_name):
	print("User won rising flag: ", flag_name)
	emit_signal("win", self.world)

func _on_kill_area_body_enter( body ):
	if(body.get_name() == "player"):
		print("User died")
		emit_signal("die", world)
