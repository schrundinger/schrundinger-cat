extends Control

signal game_over

const VECTOR_0 = Vector2(0, 0)
const SPLIT_SECONDS = 1
var last_split = 0
var all_worlds = []
var active_worlds = []
var delete_worlds = []
const MAX_WORLDS = 4

onready var container_size = get_node(".").get_size()
onready var container_center = Vector2(container_size.x/2, container_size.y/2)

export(PackedScene) var scene_file

func _ready():
	set_process(true)
	for child in get_node("worlds").get_children():
		all_worlds.append("worlds/" + child.get_name())
	var world1 = all_worlds[0]
	active_worlds = [world1]
	var viewport1 = get_node(world1).get_node("viewport")
	var scene = scene_file.instance()
	viewport1.add_child(scene)
	scene.world = world1
	scene.connect("die", self, "_on_stage_die")
	scene.connect("win", self, "_on_stage_win")

func _process(delta):
	var must_resize = false;
	if delete_worlds.size() > 0:
		var todelete = delete_worlds[0]
		for world in delete_worlds:
			delete_stage(world)
		check_gameover()
		delete_worlds = []
		must_resize = true;
	if last_split > 0:  # debouncing safeguard
		last_split -= delta
	elif Input.is_action_pressed("split") and can_split():
		do_split()
		must_resize = true
		
	if must_resize:
		resize_worlds()

func check_gameover():
	if active_worlds.size() < 1:
		print("GAMEOVER")
		get_node("/root/global").goto_scene("res://screens/game_over.tscn")


func delete_stage(world):
	var i = active_worlds.find(world)
	if i >= 0:
		active_worlds.remove(i)
		var world_node = get_node(world)
		world_node.get_node("viewport/stage").free()
		world_node.set_pos(container_size) # out of canvas

func do_split():
	last_split = SPLIT_SECONDS
	var copy_worlds = []
	for world in active_worlds:
		copy_worlds.append(world)
	for source_world in copy_worlds:
		do_split_world(source_world)

func do_split_world(source_world):
	var target_world = get_an_empty_world()
	var old_stage = get_node(source_world).get_node("viewport/stage")
	var new_stage = old_stage.duplicate(false)
	new_stage.connect("die", self, "_on_stage_die")
	new_stage.connect("win", self, "_on_stage_win")
	new_stage.world = target_world
	get_node(target_world).get_node("viewport").add_child(new_stage)
	old_stage.get_node("player").force_jump()
	var i = active_worlds.find(source_world)
	active_worlds.insert(i, target_world)

func can_split():
	return active_worlds.size() * 2 <= MAX_WORLDS

func get_an_empty_world():
	for world in all_worlds:
		if not (world in active_worlds):
			return world
			
func resize_worlds():
	var num_active_worlds = active_worlds.size()
	var world_heigth = container_size.y/num_active_worlds
	
	# FIXME: Magic numbers, will fail if we change game size
	var camera_zoom = 1+((num_active_worlds-1)*0.5)
	var vec_camera_zoom = Vector2(camera_zoom, camera_zoom)
	var vec_camera_offset = Vector2(150+((num_active_worlds-1)*100), 0)

	for i in range(num_active_worlds):
		var world = get_node(active_worlds[i])

		world.set_pos(container_center)
		world.set_size(VECTOR_0)
		
		var final_pos = Vector2(0, i*world_heigth)
		var final_size = Vector2(container_size.x, world_heigth)
		
		world.tween_to_rect(final_pos, final_size)
		
		var camera = world.get_node("viewport/stage/player/camera")
		camera.set_zoom(vec_camera_zoom)
		camera.set_offset(vec_camera_offset)
		
	var p2 = self.get_node("particles2")
	var p3_1 = self.get_node("particles3_1")
	var p3_2 = self.get_node("particles3_2")
	var p4_1 = self.get_node("particles4_1")
	var p4_2 = self.get_node("particles4_2")
	p2.set_hidden(true)
	p3_1.set_hidden(true)
	p3_2.set_hidden(true)
	p4_1.set_hidden(true)
	p4_2.set_hidden(true)
	if num_active_worlds == 3:
		p3_1.set_hidden(false)
		p3_2.set_hidden(false)
	elif num_active_worlds == 4:
		p4_1.set_hidden(false)
		p4_2.set_hidden(false)
	if num_active_worlds == 2 or num_active_worlds == 4:
		p2.set_hidden(false)


func _on_stage_die(world):
	delete_worlds.append(world)

func _on_stage_win(world):
	get_node("/root/global").goto_scene("res://screens/win.tscn")
	