extends Control

var tween

func _ready():
	tween = Tween.new()
	self.add_child(tween)
	
func tween_to_rect(pos, size):
	tween.interpolate_method(self, "tweening_pos", self.get_pos(), pos, 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.interpolate_method(self, "tweening_size", self.get_size(), size, 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	
func tweening_pos(pos):
	self.set_pos(pos)

func tweening_size(size):
	self.set_size(size)
