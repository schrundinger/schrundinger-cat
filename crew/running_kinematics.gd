
extends KinematicBody2D

const GRAVITY = Vector2(0, 1500)
export var SPEED = 200
export var JUMP = 700
const FLIGHT_ACCELERATION = Vector2(100, 0)

onready var micro_fall = GRAVITY.normalized()
onready var jump = - JUMP * micro_fall

var v = Vector2()
var direction = 0
var jump_requested

func _ready():
	set_fixed_process(true)

func run_right():
	direction = 1

func run_left():
	direction = -1

func run_stop():
	direction = 0

func run_reverse():
	direction *= -1

func please_jump():
	if _on_floor():
		jump_requested = true
	return jump_requested

func force_jump():
	if not please_jump():
		_do_jump()

func _fixed_process(dt):
	if _on_floor():
		_do_run(dt)
	else:
		_do_fall(dt)

func _do_run(dt):
	# collide with the floor surface
	move(micro_fall)
	var surface = get_collision_normal()
	v = - direction * SPEED * surface.tangent().normalized()
	if jump_requested:
		_do_jump()
	_slide_on_floor(dt)

func _do_fall(dt):
	v  += GRAVITY * dt
	if abs(v.x) < SPEED:
		v += FLIGHT_ACCELERATION * dt
	_slide_on_air(dt)

func _slide_on_floor(dt):
	var dx = v * dt
	dx = move(dx)
	if is_colliding():
		var n = get_collision_normal()
		# Projecting over the normalized slide instead of sliding
		# will keep the modulus of the speed
		# but will look like a strong climb on strong uphills
		# and a strong grip on strong downhills
		v = v.length() * n.slide(v).normalized()
		dx = dx.length() * n.slide(dx).normalized()
		# Do not move if there is no movement,
		# as it is invalidating the collision list
		# for calls to is_colliding() outside _fixed_process loop
		if dx.length() > 0:
			move(dx)
			# maybe keep trying to slide and move
			# until dx.length() = 0

func _slide_on_air(dt):
	var dx = v * dt
	dx = move(dx)
	if is_colliding():
			var n = get_collision_normal()
			var mod = dx.length()
			dx = n.slide(dx)
			v = n.slide(v)
			move(dx)

func _do_jump():
	# Substract the upwards velocity from the jump
	v += jump - v.dot(micro_fall) * micro_fall
	jump_requested = false

func _on_floor():
	return test_move(micro_fall)
