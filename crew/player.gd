extends "res://crew/running_kinematics.gd"

var roll = false
onready var anim = 'run'
onready var animation = get_node("anim")

func _ready():
	set_process(true)
	self.run_right()
	
func _process(delta):
	_input()
	_animate()

func _input():
	if Input.is_action_pressed("jump"):
		please_jump()
	roll = Input.is_action_pressed("ui_down")

func _animate():
	var next_anim = 'run'
	if not _on_floor():
		next_anim = 'jump'
	if roll:
		next_anim = 'roll'

	if anim != next_anim:
		anim = next_anim
		animation.play(anim)