extends "res://crew/running_kinematics.gd"

signal touched(body)

export var span = 200
const REVERSE_DELAY = 1
onready var reversed = 0
onready var initial_position = get_global_pos()

func _ready():
	get_node("anim").play('run')
	set_process(true)
	self.run_right()
	
func _process(delta):
	if reversed > 0:
		reversed -= delta
	elif _out_of_bounds() or is_colliding():
		reversed = REVERSE_DELAY
		self.run_reverse()

func _out_of_bounds():
	var current_position = get_global_pos()
	return abs(current_position.x - initial_position.x) > span
	
func _on_body_enter( body ):
	emit_signal("touched", body)