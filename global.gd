extends Node

var current_scene = null

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child( root.get_child_count() -1 )

func goto_scene(path):
	# This function will usually be called from a signal callback,
	# or some other function from the running scene.
	# Deleting the current scene at this point might be
	# a bad idea, because it may be inside of a callback or function of it.
	# The worst case will be a crash or unexpected behavior.

	# The way around this is deferring the load to a later time, when
	# it is ensured that no code from the current scene is running:

	call_deferred("_deferred_goto_scene", path)

func goto_scene_instance(instance):
	call_deferred("_deferred_goto_scene_instance", instance)
	
func goto_splitted_scene(path):
	var lvl = ResourceLoader.load(path)
	var splitter = ResourceLoader.load("res://levels/splitter.tscn").instance()
	splitter.scene_file = lvl
	goto_scene_instance(splitter)

func _deferred_goto_scene(path):
	# Load new scene
	var s = ResourceLoader.load(path)

	# Instance the new scene
	var instance = s.instance()

	_deferred_goto_scene_instance(instance)


func _deferred_goto_scene_instance(scene_instance):
	# Immediately free the current scene,
	# there is no risk here.
	current_scene.free()

	# Instance the new scene
	current_scene = scene_instance

	# Add it to the active scene, as child of root
	get_tree().get_root().add_child(current_scene)

	# optional, to make it compatible with the SceneTree.change_scene() API
	get_tree().set_current_scene(current_scene)
