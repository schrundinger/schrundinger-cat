extends Node

var MAX_LEVEL = 8

func _on_ButtonGroup_button_selected( button ):
	var open_level = button.get_text()
	if not open_level.casecmp_to("-"):
		get_node("/root/global").goto_scene("res:///screens/select_level.tscn")
	elif int(open_level) <= MAX_LEVEL:
		get_node("/root/global").goto_splitted_scene("res://levels/level"+open_level+".tscn")
	else:
		get_node("/root/global").goto_splitted_scene("res://levels/level3.tscn")
		
	
	
